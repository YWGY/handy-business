import React, {Component, Fragment} from 'react';

class BusinessDisplay extends Component{
    constructor(props){
        super(props);
        this.state={
            inputValue:'',
            list:[]
        }
    }
    render(){
        return(
        <Fragment>
            <div>
                <input 
                value={this.state.inputValue} 
                onChange={this.handleInputChange.bind(this)}/>
                <button onClick={this.handleButtonClick.bind(this)}>Submit</button>
            </div>
            <ul>
                {
                   this.state.list.map((item, index)=>{
                       return (
                       <li 
                       key={index} 
                       onClick={this.handleItemDelete.bind(this.index)}>
                       {item}
                       </li>)//这里的index最好要改
                   })
                }
            </ul>
        </Fragment>
        )
    }
    handleInputChange(event){
        this.setState({
            inputValue:event.target.value
        })
        // this.state.inputValue=event.target.value;
    }
    handleButtonClick(){
        this.setState({
            list:[...this.state.list, this.state.inputValue],
            inputValue:'' //展开运算符，把数组的内容展开拼成全新的数组
        })
    }
    handleItemDelete(index){
        //immutable state不允许做任何的改变
        const list = [...this.state.list]; //list是数组[]的拷贝
        list.splice(index,1);  //splice() 方法通过删除或替换现有元素或者原地添加新的元素来修改数组,并以数组形式返回被修改的内容。此方法会改变原数组。
        this.setState({
            list:list   //把list变为当前list的变量
        })
    }
}

export default BusinessDisplay;





// import React, { Component } from 'react';
// import Button from '../UI/Button';

// export default class BusinessDisplay extends Component {
//     constructor(props) //构造函数 优于其他任何函数 
//     {   super(props);//指代component这个类，调用super props
//         this.state = {business:'',
//         list:[]
//     };//组件状态 
//     }
//   render() {
//     return (
//         <fragment>
//         <form className="BusinessD-edit">
//             <div className="BusinessD">
//                 <div className="form-group">
//                     <label>Business Code</label>
//                     <input
//                     className="form-control"
//                     // name="code"
//                     value={this.state.business}
//                     onChange={this.handleInputChange}
//                     />
//                 </div>
//                 <div className="row">
//                 <div className="col-md-12">
//                 <div className="form-group">
//                     <label>Introduction</label>
//                     <textarea
//                     style={{ height: 100 }}
//                     className="form-control"
//                     // name="description"
//                     value={this.state.business}
//                     onChange={this.handleInputChange}
//                     />
//                 </div>
//                 </div>
//                 </div>
//             </div>
//         <input type="submit" value="Submit" />
//         <Button> Cancel </Button>
//         </form> 
//         </fragment>
//     )
//   }
//   handleInputChange(e){
//       console.log(e.target);
//   }
// }