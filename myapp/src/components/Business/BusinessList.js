import React from 'react';
import axios from 'axios';

export default class BusinessList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {value: 'Please enter'};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
}
handleChange(event) {
  this.setState({value: event.target.value});
}

handleSubmit(event) {
  const business = this.businessInput.value;
  alert('The business list was submitted: ' + business);
  event.preventDefault();
  axios
  .post('/businesses', this.state)
  .then(response => {
    console.log(response)
  })
  .catch(error => {
    console.log(error)
  })
}

render() {
  return (
    <div className="col-sm-offset-2 col-sm-10">
       <form onSubmit={this.handleSubmit}>

       <div>
         <h2>Business List</h2>
         <p>Please enter your business list</p>
       </div>

        <div className="form-group">
        
          <label htmlFor="InputBName">Business Name</label>
          <input type="text" className="form-control" id="InputBName" placeholder="Business Name" />
       
          <label htmlFor="InputEmail1">Email address</label>
          <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
          <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
       
          <label htmlFor="InputABN">ABN</label>
          <input type="text" className="form-control" id="InputABN" placeholder="ABN" />
          
          <label htmlFor="InputPhone">Phone</label>
          <input type="number" className="form-control" id="InputPhone" placeholder="Phone" />

          <label htmlFor="InputStreet">Street Address</label>
          <input type="text" className="form-control" id="InputStreet" placeholder="Street Address" />

          <label htmlFor="InputPostcode">Postcode</label>
          <input type="number" className="form-control" id="InputPostcode" placeholder="Postcode" />

          <label htmlFor="InputState">State</label>
          {/* <select value={this.state.value} onChange={this.handleChange}>
            <option value="QLD">QLD</option>
            <option value="TAS">TAS</option>
            <option value="WA">WA</option>
          </select> */}
          <input type="text" className="form-control" id="InputState" placeholder="State" />
        </div>
        <div className="form-check">
          <input type="checkbox" className="form-check-input" id="exampleCheck1" />
          <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
        </div>
      <input type="submit" value="Submit" />
     </form> 
    </div> 
  );
 }
}
