import React from 'react';
import {NavLink} from 'react-router-dom';

function Header() {
    return (
<div className="navbar-header">
<nav className="navbar navbar-expand-sm bg-light">
<ul className="navbar-nav">
    <li className="nav-item">
    <NavLink className='nav-link' to='/'> Home <span className='sr-only'>(current)</span> </NavLink>
    </li>
    <li className="nav-item">
    <NavLink className='nav-link' to='/List'> List <span className='sr-only'>(current)</span> </NavLink>
    </li>
    <li className="nav-item">
    <NavLink className='nav-link' to='/Display'> Display <span className='sr-only'>(current)</span> </NavLink>
    </li>
    <li className="nav-item">
    <NavLink className='nav-link' to='/Check'> Check <span className='sr-only'>(current)</span> </NavLink>
    </li>
  </ul>
</nav>
</div>
    )
  }

export default Header;
