import React from 'react'; //标签需要JSX语法，引入react
import {BrowserRouter, Route, Switch} from 'react-router-dom'; 

import List from '../Business/BusinessList';
import Header from '../Business/Header';
import Home from '../Business/BusinessHome';
import Display from '../Business/BusinessDisplay';
import Check from '../Business/BusinessCheck';
class Routes extends React.Component{
    render() {
        return (
         
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route path='/List' component={List}/>
                        <Route path='/Display' component={Display}/>
                        <Route path='/Check' component={Check} />
                    </Switch>
                </div>         
            </BrowserRouter>
         
        );
      }
}
export default Routes;