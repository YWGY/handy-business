import React from 'react'; //标签需要JSX语法，引入react
import Routes from './components/App/Routes';

class App extends React.Component{
    render() {
        return (
         <div>
             <main className="container">
                 <Routes />
             </main>
         </div>
         
        );
      }
}
export default App;
